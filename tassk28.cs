﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please type in three words :)");

            var a = Console.ReadLine();

            var output = a.Split(' ');

            Console.WriteLine($"{output[0]}");
            Console.WriteLine($"{output[1]}");
            Console.WriteLine($"{output[2]}");

        }
    }
}
