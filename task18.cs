﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();
            names.Add(Tuple.Create("Megan", 19));
            names.Add(Tuple.Create("Taylor", 20));
            names.Add(Tuple.Create("Damo", 58));

            Console.WriteLine($" here is the list of names and ages :         {names[0].Item1},{names[0].Item2}. {names[1].Item1},{names[1].Item2} and {names[2].Item1},{names[2].Item2}. ");
        }
    }
}
