﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi, Please give me the price of your first item..");

            var a = Double.Parse(Console.ReadLine());

            Console.WriteLine("Thanks now what is the price of your second item?");

            var b = Double.Parse(Console.ReadLine());

            Console.WriteLine("And lastly for your third items price?..");

            var c = Double.Parse(Console.ReadLine());

            Console.WriteLine($"The total cost of your items, including GST are: ${ (a + b + c)*1.15}  ");
        }
    }
}
