﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello, please select 1 for kilometers to miles conversion and 2 for miles to kilometers.");

            var ConversionSelection = Console.ReadLine();

            switch (ConversionSelection)
            {
                case "1":
                    Console.WriteLine("okay please tell me your quantity in kilometers");
                    var a = double.Parse(Console.ReadLine());
                    Console.WriteLine($"Thanks, {a} kms, is converted to {a * 1.6} miles.");
                    break;

                case "2":
                    Console.WriteLine("thankyou please tell me your quantity in miles");
                    var b = double.Parse(Console.ReadLine());
                    Console.WriteLine($"Thanks, {b} miles, is converted to {b / 1.6} kilometers");
                    break;
            }
        }
    }
}