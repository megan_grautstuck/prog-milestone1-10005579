﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Heloooo, please give me your first number...");
            var a = Double.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine(" Thanks, now may i have the second number...");
            var b = Double.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine(" And now for your last number...");
            var c = Double.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine(" Here are your maths questions !");
            Console.WriteLine($"Question 1:   {a} x {b} + {c} = ?                       Answer = {a * b + c}");
            Console.WriteLine($"Question 2:   {c} x {a} / {c} = ?                       Answer = {c * a / c }");
            Console.WriteLine($"Question 3:   {c} / {b} * {a} = ?                       Answer = {c / b * a}");
            Console.WriteLine($"Question 4:   {a} + {a} + {a} = ?                       Answer = {a + a + a}");
            Console.WriteLine($"Question 5:   {c} + {a} / {b} = ?                       Answer = {c + a / b} ");


            Console.WriteLine("                    Thankyoooooooooooou");

        }
    }
}
