﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_11
{
    class Program
    {
        static void Main(string[] args)
        {

           Console.WriteLine("Please enter the year you want to check is a Leap or not");

            var year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
                Console.WriteLine("yes");

            else if (year % 100 == 0)
                Console.WriteLine("no");

            else if (year % 4 == 0)
                Console.WriteLine("yes");

            else
                Console.WriteLine("no");
        }
    }
}
