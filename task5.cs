﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Helloo, what is the time in 24hr time? (Please write in xx.xx (e.g: 12.34) format)");

            var a = double.Parse(Console.ReadLine());

            if (a > 12)
            {
                Console.WriteLine($" thanks, that is {a - 12}pm");
            }

            else if(a < 12)
            { Console.WriteLine($" thanks, that is {a}am");

            }

            else
            {
                Console.WriteLine("sorry that is an invalid time..");
            }

            Console.ReadLine();

        }
    }
}
